/**
 * Created by ika on 02/02/17.
 */
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                compress: {
                    global_defs: {
                        'DEBUG': true
                    },
                    dead_code: true
                },
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: [
                    'src/js/Chart.bundle.js',
                    'bower_components/jquery/dist/jquery.js',
                    'src/js/bootstrap.min.js',
                    'src/js/jquery.dlmenu.js',
                    'src/js/swiper.min.js',
                    'src/js/modernizr.custom.js',
                    'src/js/jquery.easypiechart.js',
                    'bower_components/typed.js/dist/typed.min.js'
                ],
                dest: 'dist/js/global.js'
            }
        }
    });


    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task(s).
    grunt.registerTask('default', ['uglify']);

};
