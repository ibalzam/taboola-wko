
var currentChart = 0;

function goToNextChart(c) {
    currentChart = currentChart + c;
    if (!country_data[currentChart]) { currentChart = 0; }

    myChart.data.datasets = country_data[currentChart].data;
    $(".chart-country-name").fadeOut("", function() {
        $(".chart-country-name").text(country_data[currentChart].country);
        $(".chart-country-name").fadeIn();
    })

    myChart.update();
}

var color_blue_lightblue_alpha = 'rgba(20, 73, 153, 0.4)';
var color_blue_lightblue = 'rgba(20, 73, 153, 1)';

var color_blue_light_alpha = 'rgba(4, 161, 202, 0.4)';
var color_blue_light = 'rgba(4, 161, 202, 1)';

var color_green_alpha = 'rgba(99, 199, 134,0.4)';
var color_green = 'rgba(99, 199, 134,1)';

var color_white = 'rgba(255,255,255, 1)';
var color_yellow = 'rgba(254, 201, 71,1)';
var color_orange = 'rgba(253, 147, 12,1)';
var country_data = [{
    country: 'Israel',
    data: [
        {

            label: 'Desktop',
            data: [27134,24800,23198,24380,29878,42385,60371,74202,80669,84955,86179,87376,90903,90649,87404,78247,71281,70188,74016,75169,69922,60805,45987,33708],
            backgroundColor: [
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
            ], borderWidth: 0
        },
        {
            label: 'Mobile',
            data: [8710,6031,5661,8156,18840,28337,32378,31257,31819,32847,34491,36487,37802,38915,39013,39157,37903,39531,42768,44739,45211,37760,25271,14533],
            backgroundColor: [
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
            ], borderWidth: 0
        },
        {
            label: 'Tablet',
            data: [711,541,572,1068,2029,3012,2888,2681,2246,2428,2603,2919,3149,3108,3333,3509,3161,3569,3897,4750,4917,3942,2553,1276],
            backgroundColor: [
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
            ], borderWidth: 0
        }]
}, {
    country: 'United States',
    data: [
        {

            label: 'Desktop',
            data: [892341,633659,487537,433032,522601,794354,1304644,2035293,2671016,2974279,3139108,3263355,3226944,3193094,3174682,3098948,2736114,2403674,2301701,2280209,2211931,2013773,1691492,1254596],
            backgroundColor: [
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
            ], borderWidth: 0
        },
        {
            label: 'Mobile',
            data: [866236,589592,413205,347864,416949,661952,973894,1206314,1391712,1569699,1593505,1561947,1582347,1520909,1484929,1537014,1581991,1625815,1665567,1812150,1972294,1967167,1710518,1258102],
            backgroundColor: [
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
            ], borderWidth: 0
        },
        {
            label: 'Tablet',
            data: [167759,105571,70385,64480,86655,145527,217842,272329,282852,272029,257071,248081,245994,245215,260192,286026,307300,330642,367954,429630,483345,473742,395482,264428],
            backgroundColor: [
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
            ], borderWidth: 0
        }]
},{
    country: 'United Kingdom',
    data: [
        {

            label: 'Desktop',
            data: [83390,54709,42004,38869,46464,81154,166336,288523,395314,420150,439518,506316,514469,469877,467510,495750,430434,393041,403711,407550,382145,326837,222944,139267],
            backgroundColor: [
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
            ], borderWidth: 0
        },
        {
            label: 'Mobile',
            data: [108901,67256,53168,54820,84051,225109,400367,382574,342374,309812,289473,324036,343735,314448,325848,376109,426964,466617,472239,519166,579417,599359,407991,215792],
            backgroundColor: [
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
            ], borderWidth: 0
        },
        {
            label: 'Tablet',
            data: [33231,21157,18658,20901,32861,85959,145084,150174,128597,99754,85249,89614,95014,93163,98687,125943,142514,160471,167886,197160,216765,223892,146346,71713],
            backgroundColor: [
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
                color_white,color_white,color_white,color_white,color_white,color_white,
            ], borderWidth: 0
        }]
},
    {
        country: 'Germany',
        data: [
            {

                label: 'Desktop',
                data: [28739,19658,19394,26418,46854,94968,157175,202399,220407,231295,237454,230813,230895,228890,233831,233476,224902,230249,227344,211390,172172,117024,69429,40886],
                backgroundColor: [
                    color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,color_blue_lightblue,
                ], borderWidth: 0
            },
            {
                label: 'Mobile',
                data: [12274,8749,9457,16558,34663,50139,54954,60672,56465,57342,62726,64835,62347,63858,66137,69063,69657,75078,86532,94717,88630,65014,39300,20448],
                backgroundColor: [
                    color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                    color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                    color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                    color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,color_blue_light,
                ], borderWidth: 0
            },
            {
                label: 'Tablet',
                data: [1908,1587,1588,3594,6585,10365,10809,9771,9184,8423,9180,9803,9630,9708,10735,11668,12699,15153,17310,19981,18041,12260,6992,3083],
                backgroundColor: [
                    color_white,color_white,color_white,color_white,color_white,color_white,
                    color_white,color_white,color_white,color_white,color_white,color_white,
                    color_white,color_white,color_white,color_white,color_white,color_white,
                    color_white,color_white,color_white,color_white,color_white,color_white,
                ], borderWidth: 0
            }]
    },
    {
        country: 'Japan',
        data: [
            {
                label: 'Desktop',
                data: [217346,136663,107707,112632,170191,288895,545974,1064348,1188869,1194137,1286560,2175593,1211335,1099055,1198388,1258506,1193958,905131,729078,745534,786163,709807,555481,353986],
                backgroundColor: [
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,
                ],
                borderWidth: 0
            },
            {
                label: 'Mobile',
                data: [674538,420343,319232,315304,581324,1170259,1630077,1570792,1226964,1130971,1168298,1974770,1349490,1183730,1227495,1209264,1345272,1471246,1453966,1658203,1889044,1882861,1634966,1111470],
                backgroundColor: [
                    color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                    color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light,
                    color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                    color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                ],

                borderWidth: 0
            },
            {
                label: 'Tablet',
                data: [52734,32425,26095,29709,50470,87510,117319,115512,110506,101503,102605,122444,109928,101435,107316,114644,120671,122604,125692,156149,183349,178982,146709,90627],
                backgroundColor: [
                    color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                    color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green,
                    color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                    color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                ],

                borderWidth: 0
            }]
    }];

var myChartCtx = document.getElementById("myChart");
var myChart = new Chart(myChartCtx, {
    type: 'bar',
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
        datasets: country_data[currentChart].data
    },
    options: {
        animation: {
            duration: 2500,
            // easing: 'easeOutQuart'
        },
        scales: {
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero:true,
                    autoSkip:true,
                    // stepSize: 100,
                    fontColor: '#d6d8d7',
                    fontSize: 16,
                    fontStyle: 'bold',
                },
                gridLines: {
                    // tickMarkLength: 15,
                    display: true,
                    color: "rgba(54,58,61,0.5)"
                },
            }],
            xAxes: [{
                categoryPercentage: 1.0,
                barPercentage: 1.0,
                padding: 0,
                stacked: true,
                gridLines: {
                    display: false,
                    color: "rgba(54,58,61,0.5)"
                },
                ticks: {
                    fontColor: '#d6d8d7',
                    fontSize: 16,
                    fontStyle: 'bold',
                    beginAtZero:true
                }
            }]

        },
        legend: {
            position: 'bottom',
            labels: {
                fontColor: '#d6d8d7',
                fontStyle: 'bold',
                fontFamily: 'HelveticaLTStd'
            }
        }
    }
});

var dcChartCtx = document.getElementById("dcChart");
var dcChart = new Chart(dcChartCtx, {
    type: 'bar',
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,"","",1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
        datasets: [
            {

                label: 'Desktop',
                data: [27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,0,0,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198],
                backgroundColor: [
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                ], borderWidth: 0
            },
            {
                label: 'Mobile',
                data: [27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,0,0,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198],                backgroundColor: [
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
            ], borderWidth: 0
            },
            {
                label: 'Tablet',
                data: [27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,0,0,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198],                backgroundColor: [
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
            ], borderWidth: 0
            }]
    },
    options: {
        animation: {
            duration: 2500,
            // easing: 'easeOutQuart'
        },
        scales: {
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero:true,
                    autoSkip:true,
                    // stepSize: 100,
                    fontColor: '#000',
                    fontSize: 16,
                    fontStyle: 'bold',
                },
                gridLines: {
                    // tickMarkLength: 15,
                    display: true,
                    color: "rgba(54,58,61,0.5)"
                },
            }],
            xAxes: [{
                categoryPercentage: 1.0,
                barPercentage: 1.0,
                padding: 0,
                stacked: true,
                gridLines: {
                    display: false,
                    color: "rgba(54,58,61,0.5)"
                },
                ticks: {
                    fontColor: '#000',
                    fontSize: 16,
                    fontStyle: 'bold',
                    beginAtZero:true
                }
            }]

        },
        legend: {
            position: 'bottom',
            labels: {
                fontColor: '#000',
                fontStyle: 'bold',
                fontFamily: 'HelveticaLTStd'
            }
        }
    }
});







/* Dating */
var dtChartCtx = document.getElementById("dtChart");
var dtChart = new Chart(dtChartCtx, {
    type: 'bar',
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
        datasets: [
            {

                label: 'Desktop',
                data: [27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,0,0,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198],
                backgroundColor: [
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                    color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue_alpha,color_blue_lightblue,color_blue_lightblue,
                ], borderWidth: 0
            },
            {
                label: 'Mobile',
                data: [27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,0,0,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198],                backgroundColor: [
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
                color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,color_blue_light_alpha,
            ], borderWidth: 0
            },
            {
                label: 'Tablet',
                data: [27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,0,0,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198,27134,24800,23198],                backgroundColor: [
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
                color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,color_green_alpha,
            ], borderWidth: 0
            }]
    },
    options: {
        animation: {
            duration: 2500,
            // easing: 'easeOutQuart'
        },
        scales: {
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero:true,
                    autoSkip:true,
                    // stepSize: 100,
                    fontColor: '#000',
                    fontSize: 16,
                    fontStyle: 'bold',
                },
                gridLines: {
                    // tickMarkLength: 15,
                    display: true,
                    color: "rgba(54,58,61,0.5)"
                },
            }],
            xAxes: [{
                categoryPercentage: 1.0,
                barPercentage: 1.0,
                padding: 0,
                stacked: true,
                gridLines: {
                    display: false,
                    color: "rgba(54,58,61,0.5)"
                },
                ticks: {
                    fontColor: '#000',
                    fontSize: 16,
                    fontStyle: 'bold',
                    beginAtZero:true
                }
            }]

        },
        legend: {
            position: 'bottom',
            labels: {
                fontColor: '#000',
                fontStyle: 'bold',
                fontFamily: 'HelveticaLTStd'
            }
        }
    }
});