/**
 * Created by ika on 07/02/17.
 */

/* Globals */
var swiper;
var currentSlide = 0;
var face = true;
var $infoPop, globeApi;
var cycledSpotIndex = 0;
var eCommerce = 1;
var internal = null;
var videoFirstRun = true;
var facesCounter = 0;


/* Globe Configuration */
function globeFactory(el, config) {
    var rotationOn = false;
    var globeProxy = {
        goToPointIndex: function (){},
        startRotation: function (){
            rotationOn = true;
        },
        stopRotation: function (){
            rotationOn = false;
        }
    };
    var loadInterval = setInterval(function () {
        if (typeof createGlobe === 'undefined') return;
        clearInterval(loadInterval);
        var api = createGlobe(el, config);
        globeProxy.goToPointIndex = api.goToPointIndex.bind(api);
        globeProxy.startRotation = api.startRotation.bind(api);
        globeProxy.stopRotation = api.stopRotation.bind(api);

        if (rotationOn) {
            globeProxy.startRotation();
        }

    },100);

    return globeProxy;
}


function numberCommas(num) {
    return num
        .toString()
        .split('')
        .reverse()
        .reduce(function (arr, char, index) {
            if (index > 0 && index % 3 === 0) {
                arr.push(',');
            }

            arr.push(char);

            return arr;
        }, [])
        .reverse()
        .join('');
}




/* Onload */
$(function() {

    // Add page numbers
    $(".pt-page").each(function(i, e) {
        $(e).addClass("pt-page-" + i+1);
    });
    // Select the first chart
    goToNextChart(0);


    /* Typed */
    $(".element").typed({
        strings: ["Shock", "Surprise", "Believe", "Reveal", "Secret"],
        typeSpeed: 30,
        loop: true,
        backDelay: 2500,
        startDelay: 3000
    });



    // Number lang groups
    $(".lang-group").each(function(i, item) {
        $(item).attr('data-lang-id', i);
    });

    $('.lang-chart').easyPieChart({
        barColor: '#fee830',
        lineWidth: 50,
        size: 630,
        trackColor: false,
        scaleColor: false,
        lineCap: 'square'
    });

    $('.lang-group').click(function() {
        changeToLanguage(this);
    });



    $(document).bind('keydown', function(e) {
        if (e.which == 38){ // back
            doNextInternal(-1);
        }
        if (e.which == 40) { // next
            doNextInternal(1);
        }
    });

    $infoPop = $('.js-info-pop');
    $infoPop.css('opacity', 0);

    var $countryName = $infoPop.find('.js-country-name');
    var $clickCount = $infoPop.find('.js-click-count');
    var $popSize = $infoPop.find('.js-pop-size');
    var $clicksPerPerson = $infoPop.find('.js-clicks-per-person');

    var globeContainer = document.getElementById('globe-container');
    var $globeContainer = $(globeContainer);
    globeApi = globeFactory(globeContainer, {
        data: country_click_data,
        callbacks: {
            selection: function (selectedSpot) {
                var mData = selectedSpot.metadata;
                $infoPop.show();
                $infoPop.css('opacity', 1);

                $countryName.text(mData.countryName.toUpperCase());
                $clickCount.text(numberCommas(mData.totalClicks));
                $popSize.text(numberCommas(mData.population));
                $clicksPerPerson.text((mData.totalClicks / mData.population).toFixed(3));

                $globeContainer.one('mousedown', function () {
                    $infoPop.hide();
                    $infoPop.css('opacity', 0);
                });
            }
        }
    });


    // Start the globe rotation
    globeApi.startRotation();

    swiper = new Swiper('.swiper-container', {
        pagination: null,
        slidesPerView: 2.1,
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: false,
        autoplayDisableOnInteraction: false,
//			keyboardControl: true,
        loop: true,
        onSlideChangeStart: function(x) {
            var active = $(".swiper-slide-active");
            $(".slider-country-name").fadeOut("",function() {
                $(".slider-country-name").text(active.data("country"));
                $(".slider-country-name").fadeIn();
            });
        }
    });

});

function crossfade(e1, e2) {
    $(e1).fadeOut("",function() {
        $(e2).fadeIn();
    });
}

function gotoNextOffice() {
    $infoPop.hide();
    $infoPop.css('opacity', 0);
    globeApi.goToPointIndex(cycledSpotIndex);

    cycledSpotIndex++;
    if (cycledSpotIndex >= country_click_data.length) {
        cycledSpotIndex = 0;
    }
}

/* Languages slide */
function changeToLanguage(next, direction) {
    if (!next) {
        var currentId = $('.lang-active').data('lang-id');
        next = $('*[data-lang-id="' + (currentId + direction) + '"]');
        if (!next.length) {
            next = $('*[data-lang-id="0"]');
        }
    }

    $('.lang-active').removeClass('lang-active');
    $(next).addClass('lang-active');
    $('.lang-chart').data('easyPieChart').update($(next).data('percent'));
}

/* Faces slide */
function faces() {
    facesCounter++;
    switch (facesCounter) {
        case 1:
            $(".face-title-1").fadeOut(function() {
                $(".face-title-2").fadeIn();
                $(".image-number").css('visibility', 'visible');
            });
            break;
        case 2:
            $(".all-faces").fadeOut("", function() {
                $(".one-face").fadeIn("");
            });
            break;
    }
}

/* Internal click handler */
function doNextInternal(direction) {
    switch (getSlideName()) {
        case "faces":
            faces();
            break;
        case "click-charts":
            goToNextChart(direction);
            break;
        case "languages":
            changeToLanguage(null, direction);
            break;
        case "globe":
            $("#globe .globe-title").fadeOut();
            globeApi.stopRotation();
            gotoNextOffice();
            break;
        case "ci-24":
            if (direction == 1) { swiper.slideNext(); } else { swiper.slidePrev(); }
            break;
        case "e-commerce":
            var t = eCommerce + direction;
            var el = $("#e-commerce .step-" + t);
            if (el.length) {
                $("#e-commerce .step-" + (t - direction)).fadeOut(function() {
                    el.fadeIn();
                    eCommerce = t;
                });
            }
            break;
        case "video":
            var video = document.getElementById('myVideo');
            if (video.paused) {
                if (videoFirstRun) {
                    video.src = "/video/test.mp4"; /* Workaround */
                }
                videoFirstRun = false;
                video.play();
            } else {
                video.pause();
            }
            break;
    }
}

function getSlideName() {
    return $(".pt-page-current").attr('id');
}


/************* DATA *****************/

var country_click_data = [
    {
        lng: -95,
        lat: 40,
        color: 'yellow',
        rad:2,
        metadata: {
            countryName: 'United States',
            population: 324212233,
            totalClicks: 12332111
        }
    },
    {
        lng: 50,
        lat: 60,
        color: 'red',
        rad:2,
        metadata: {
            countryName: 'Russia',
            population: 150342233,
            totalClicks: 302745
        }
    },
    {
        lng: 35.0350,
        lat: 31.8516,
        color: 'green',
        rad:0.5,
        metadata: {
            countryName: 'Israel',
            population: 7500000,
            totalClicks: 302745
        }
    },
    {
        lng: 35.0350,
        lat: 31.8516,
        color: 'green',
        rad:0.5,
        metadata: {
            countryName: 'Israel',
            population: 7500000,
            totalClicks: 302745
        }
    }
];
